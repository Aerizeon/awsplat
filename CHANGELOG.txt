v 1.5 (03/19/2012)
	* Changed score handling code to fix various memory leaks regarding deletion of score and user objects
	* Changed how scores for nonexistant users are handled; Users with scores of 0 are now removed from the list

	* Added ability to create rounds of various lengths, which is set in the configuration file
	* Added ability to have single-line comments in configuration file

v 1.4 (02/24/2012)
	* Removed World Querying system (This may reappear in a more limited form later)
	* Removed hardcoded login name, owner, password, and world (Moved to AWSplat.cfg)

	* Changed weapon range from 90 meters to 85 meters
	* Changed various functions in Query system to provide optimization

	* Added basic configuration/settings file
	* Added ability to completely terminate the bot using '/restart end'

v 1.3 (02/19/2012)
	* Removed shift-through on god
	* Removed flame creation on death

	* Changed the /god command to be self issuing (5 minute restriction on changes)
	* Changed various functions within code to use the C++ Library instead of C
	* Changed strings and string comparisons to STL strings'
	* Changed sorting algorithm to be implicitly decending, instead of using list::reverse 
	

v 1.2 (02/12/2012)
	* Removed remaining cheat management code
	* Removed bot's ability to physically enter the world

	* Changed field and safe zone code to properly detect bounds
	* Changed name to 'AWSplat Paintball'
	* Changed all messages to use console

	* Added Teleport On Enter to resolve issues where cheating could occur
	* Added limit to shooting range (Currently defaults to 90 meters)

V 1.1 (02/04/2012)
	* Removed cheat management eject code due to a bug with Activeworlds

	* Changed Field zone code to ignore Y Axis
	* Changed World of Entry from A'A to AWSplat at the request of Bluelaser5000 (Activeworlds Citizen #347619)
	* Changed internal variable types to supress compiler warnings
	* Changed project name to AWSplat

	* Added user-independent recording of scores to prevent scores from resetting on user exit
	* Added sorted score list
	* Added version message
	
v 1.0 (01/28/2012)
	* Inital release