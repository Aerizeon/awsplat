#include "Query.h"
#include <aw.h>
#include <stdio.h>

boost::signal<void ()> Query::OnQueryObject;
boost::signal<void ()> Query::OnQueryComplete;
int Query::Sequence5[5][5];
int Query::Sequence3[3][3];
int Query::MaxSectors;
int Query::CurrentX;
int Query::CurrentZ;
int Query::rc;
bool Query::Busy;
QueryType Query::Type;

void Query::Start(QueryType type, int XOrigin, int ZOrigin)
{
	Type = type;
	aw_event_set (AW_EVENT_CELL_BEGIN, Query::HandleCellBegin);
	aw_event_set (AW_EVENT_CELL_OBJECT, Query::HandleCellObject);
	aw_callback_set (AW_CALLBACK_QUERY, Query::HandleCallbackQuery);
	printf("World contains %i objects\r\n",aw_int(AW_WORLD_OBJECT_COUNT));
	memset(Sequence5,0,sizeof(Sequence5));
	memset(Sequence3,0,sizeof(Sequence3));
	MaxSectors = aw_sector_from_cell(aw_int(AW_WORLD_SIZE));
	CurrentX = CurrentZ = -MaxSectors; //Set the current position to the farthest edge of the world.
	Busy = true;
	switch(Type)
	{
		case AWQuery_5x5:
			rc =  aw_query_5x5 (CurrentX,CurrentZ, Sequence5);
			break;
		case AWQuery_3x3:
			rc =  aw_query (CurrentX,CurrentZ, Sequence3);
			break;
	}
}

void Query::Pause()
{

}

void Query::Stop()
{
	aw_event_set (AW_EVENT_CELL_BEGIN, 0);
	aw_event_set (AW_EVENT_CELL_OBJECT, 0);
	aw_callback_set (AW_CALLBACK_QUERY, 0);
}

void Query::HandleCellBegin()
{
	switch(Type)
	{
		case AWQuery_5x5:
			Sequence5[(aw_sector_from_cell (aw_int (AW_CELL_Z))-CurrentZ) + 2][(aw_sector_from_cell (aw_int (AW_CELL_X))-CurrentX) + 2] = aw_int (AW_CELL_SEQUENCE);
			break;
		case AWQuery_3x3:
			Sequence3[(aw_sector_from_cell (aw_int (AW_CELL_Z))-CurrentZ) + 1][(aw_sector_from_cell (aw_int (AW_CELL_X))-CurrentX) + 1] = aw_int (AW_CELL_SEQUENCE);
			break;
	}

}
void Query::HandleCellObject()
{
	OnQueryObject();
}
void Query::HandleCallbackQuery(int rc)
{
	if (aw_bool (AW_QUERY_COMPLETE))
	{
		if(Type == AWQuery_5x5)
			memset (Sequence5, 0, sizeof (Sequence5));
		else
			memset (Sequence3, 0, sizeof (Sequence3));

		CurrentZ+=(Type==AWQuery_5x5?5:3);
		if(CurrentZ > MaxSectors)
		{
			CurrentZ = -MaxSectors;
			CurrentX+=(Type==AWQuery_5x5?5:3);

			if(CurrentX > MaxSectors)
			{
				aw_event_set (AW_EVENT_CELL_BEGIN, 0);
				aw_event_set (AW_EVENT_CELL_OBJECT, 0);
				aw_callback_set (AW_CALLBACK_QUERY, 0);
				Busy = false;
				OnQueryComplete();
				return;
			}
		}
	}
	switch(Type)
	{
		case AWQuery_5x5:
			rc =  aw_query_5x5 (CurrentX,CurrentZ, Sequence5);
			break;
		case AWQuery_3x3:
			rc =  aw_query (CurrentX,CurrentZ, Sequence3);
			break;
	}
}

