#ifndef QUERY_H
#define QUERY_H

#include <boost/signal.hpp>
#include <boost/bind.hpp>

enum QueryType
{
    AWQuery_3x3,
    AWQuery_5x5

};

class Query
{
    public:
        static int Sequence5[5][5];
        static int Sequence3[3][3];
        static int CurrentX;
        static int CurrentZ;
        static int MaxSectors;
        static bool Busy;

        static char *QueryName; //ID or name for query
        static char *RegexModel;
        static char *RegexAction;
        static char *RegexDescription;
        static char *RegexCitizen;

        /*Interal Triggers for results*/
        static boost::signal<void ()> OnQueryObject;  //A single object matched the regex passed to the query system
        static boost::signal<void ()> OnQueryComplete;

        Query(QueryType type);
        static void Start(QueryType type, int XOrigin =0, int ZOrigin=0);
        static void Pause();
        static void Resume();
        static void Stop();
        virtual ~Query();
        static void HandleCellBegin();
        static void HandleCellObject();
        static void HandleCallbackQuery(int rc);
    protected:

    private:
        static QueryType Type;
        static int rc;

};


#endif // QUERY_H
