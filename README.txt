
== RUNTIME REQUIREMENTS ==

Include the following in the base directory of the application:
	From Activeworlds SDK (v5.2 Build 100) :
		*AW.dll
Application is preconfigured with privliges of 347619 and for entrance into AWSplat, although this can be modified in AWSplat.cfg

== COMPILE REQUIREMENTS ==

Include the following:
	From Boost (v1.47.0 Build 0):
		* <boost/signals.hpp>
	From Activeworlds SDK
		* <aw.h>

Link the following:
	From Boost (v1.47.0 Build 0):
		* libboost_signals_mgw45-mt-s-1_47.a or compatible static library
	From Activeworlds SDK (v5.2 Build 100)
		* libaw.a generated using DLLTool

Linking Options (MinGW Only)
	For the C/C++ Library 
		* -static-libstdc++
		* -static-libgcc


== Usage and Commands ==
	Administrative Commands
		/restart 	Restarts the bot
		/restart end	Terminates the bot
	User Commands
		score		Displays the user's score and KDR, as well as the current game leaders
		scores		Displays a list of all the recorded user scores
		/god		Allows the user to enter god (building) mode. Limited to 1 change per 5 minutes
		/version	Displays the version message