#include "User.h"
#include "WarBot.h"
#include <cmath>

User::User(int session)
{
    Session = session;
    Health = 0.0;
    Living = true;
    God = false;
}

bool User::Shoot(User *U)
{
    if(!U->Living)
    {
        WarBot::Debug("Deferred post-death remote shoot");
        return false;
    }
    if(!Living)
    {
        WarBot::Debug("Deferred post-death local shoot");
        return false;
    }
    CurrentWeapon = (Weapons[CurrentWeapon]?CurrentWeapon:0);
    if(!Weapons[CurrentWeapon])
    {
        WarBot::Debug("Error: Weapon ID %i Undefined",CurrentWeapon);
        return false;
    }
    float Multiplier = 1.0; //Damage percentage based on distance.
    U->Health-=Multiplier*Weapons[CurrentWeapon]->Damage;
    return true;
}
float User::Distance(User *U)
{
	int VX = U->Position.X-Position.X;
	int VZ = U->Position.Z-Position.Z;

	return sqrt((VX*VX)+(VZ*VZ))/100.0f;

}
User::~User()
{
    //dtor
}
