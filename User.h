#ifndef USER_H
#define USER_H
#include <vector>
#include <string>

#include "aw.h"
#include "Weapon.h"
#include "vector.h"

using namespace std;

class UserScore
{
public:
    string Name;
    int Citizen;
    int Kills;
    int Deaths;

    UserScore()
    {
        Kills = 0;
        Deaths = 0;
    }
    bool operator < (UserScore &U)
    {
        return Kills < U.Kills;
    }
        bool operator > (UserScore &U)
    {
        return Kills > U.Kills;
    }

};

class User
{
public:
    string Name;
	string SearchName;
    int Session;
    int Citizen;

    double Health;
    bool Living;
    UserScore *Score;
    bool God;
    unsigned int LastModeChange;
    bool Ejected;

    Vector3D Position;
    Vector3D Rotation;

    int CurrentWeapon;
    vector<Weapon*> Weapons;

    User(int session);
    bool Shoot(User *U);
    float Distance(User *U);
    virtual ~User();

protected:
private:
};

#endif // USER_H
