#include "Vector.h"
#include <cstdio>
#include <vector>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/lexical_cast.hpp>
using namespace std;

Vector2D::Vector2D(int x, int z)
{
	X=x;
	Z=z;
}
void Vector2D::Set(int x, int z)
{
	X=x;
	Z=z;
}

bool Vector2D::operator < (Vector3D &V)
{
	return ((X<V.X) && (Z<V.Z));
}
bool Vector2D::operator > (Vector3D &V)
{
	return ((X>V.X) && (Z>V.Z));
}



Vector3D::Vector3D(int x, int y, int z)
{
	X=x;
	Y=y;
	Z=z;
}
Vector3D::Vector3D(string Position)
{
	vector<string> Token;
	boost::trim(Position);
	boost::split(Token, Position, boost::is_any_of(","), boost::token_compress_on );
	if(Token.size() != 3)
		throw "Not enough params";
	X = boost::lexical_cast<int>(Token[0]);
	Y = boost::lexical_cast<int>(Token[1]);
	Z = boost::lexical_cast<int>(Token[2]);
}

void Vector3D::Set(int x, int y, int z)
{
	X=x;
	Y=y;
	Z=z;
}
bool Vector3D::operator < (Vector3D &V)
{
	return ((X<V.X) && (Y<V.Y) && (Z<V.Z));
}
bool Vector3D::operator > (Vector3D &V)
{
	return ((X>V.X) && (Y>V.Y) && (Z>V.Z));
}

bool Vector3D::operator < (Vector2D &V)
{
	return ((X<V.X) && (Z<V.Z));
}
bool Vector3D::operator > (Vector2D &V)
{
	return ((X>V.X) && (Z>V.Z));
}
