#ifndef VECTOR_H
#define VECTOR_H
#include <string>

using namespace std;

class Vector3D;

class Vector2D
{
public:
    int X,Z;
    Vector2D(int x =0, int z=0);
    void Set(int x =0, int z=0);

    bool operator < (Vector3D &V);
    bool operator > (Vector3D &V);

};

class Vector3D
{
public:
    int X,Y,Z;
    Vector3D(int x =0, int y=0, int z=0);
    Vector3D(string Position);

    void Set(int x =0, int y=0, int z=0);
    bool operator < (Vector3D &V);

    bool operator > (Vector3D &V);

    bool operator < (Vector2D &V);

    bool operator > (Vector2D &V);

};


#endif // VECTOR_H
