#include "WarBot.h"
#include "aw.h"
#include "Query.h"
#include <stdio.h>
#include <cstdarg>
#include <iostream>
#include <fstream>
#include <string>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/lexical_cast.hpp>

#define DEBUG
#define VER_MAJOR 1
#define VER_MINOR 5

using namespace std;

vector<User*> WarBot::Users;
vector<UserScore*> WarBot::Scores;
bool WarBot::SortScores = true;

Vector3D 		WarBot::GZMin;
Vector3D 		WarBot::GZMax;
Vector3D 		WarBot::FieldMin;
Vector3D 		WarBot::FieldMax;
string 			WarBot::LoginName;
string 			WarBot::LoginPPW;
string 			WarBot::LoginWorld;
unsigned int 	WarBot::LoginOwner;
unsigned int	WarBot::RoundLength;

int RoundStart = 0;

void WarBot::Start()
{
	Users.reserve(50);
	Scores.reserve(100);
	for(int i = 0; i<Users.size(); i++)
		delete Users[i];
	for(int i = 0; i<Scores.size(); i++)
		delete Scores[i];
	Users.clear();
	Scores.clear();
	try
	{
		LoadConfig();
	}
	catch (std::exception &e)
	{
		Debug("Unable to process configuration file: %s",e.what());
		Debug("Press return to exit");
		cin.ignore();
		return;
	}
	catch (const std::string &e)
	{
		Debug("Unable to process configuration file: %s",e.c_str());
		Debug("Press return to exit");
		cin.ignore();
		return;
	}

	Debug("SDK Init:\t%i",aw_init(AW_BUILD));
	Debug("SDK Connect:\t%i",aw_create(0,0,0));

	aw_event_set(AW_EVENT_AVATAR_ADD,OnUserEnter);
	aw_event_set(AW_EVENT_AVATAR_CLICK,OnUserClick);
	aw_event_set(AW_EVENT_AVATAR_CHANGE,OnUserChange);
	aw_event_set(AW_EVENT_AVATAR_DELETE,OnUserExit);
	aw_event_set(AW_EVENT_CHAT,OnChatMessage);

	aw_int_set(AW_LOGIN_OWNER,LoginOwner);
	aw_string_set(AW_LOGIN_PRIVILEGE_PASSWORD,LoginPPW.c_str());
	aw_string_set(AW_LOGIN_NAME,LoginName.c_str());
	Debug("Universe Login:\t%i",aw_login());

	aw_bool_set(AW_ENTER_GLOBAL,true);
	Debug("World Login:\t%i",aw_enter(LoginWorld.c_str()));
	aw_state_change();
	Debug("Paintball:\tNew game started");
	RoundStart = time(0);
	while(1)
	{
		aw_wait(2);
		if((RoundLength > 0) && (RoundStart+RoundLength < time(0)))
		{
			Say(0,"Round completed, restarting..");
			Say(0,"%s",GetWinList());
			Say(0,"Starting a new %i minute round in 5 seconds",(int)(RoundLength/60.0f));
			aw_wait(500);
			Restart();
			return;
		}
	}

}

User *WarBot::Find(int session)
{
	for(unsigned int i=0; i<Users.size(); i++)
	{
		if(i > Users.size()||Users[i]==NULL)
		{
			return NULL;
		}
		if(Users[i]->Session==session)
			return Users[i];
	}
	return NULL;
}

User *WarBot::Find(string Name)
{
	if(Name == "")
		return NULL;
	boost::to_upper(Name);
	for(unsigned int i=0; i<Users.size(); i++)
	{
		if(i > Users.size()||Users[i]==NULL)
		{
			return NULL;
		}
		if(Users[i]->SearchName == Name)
			return Users[i];
	}
	return NULL;
}

UserScore *WarBot::GetScore(int Citizen, string Name)
{
	if(Citizen == 0)
	{
		for(unsigned int i = 0; i<Scores.size(); i++)
		{
			if(Scores[i]->Name == Name)
			{
				return Scores[i];
			}
		}
	}
	else
	{
		for(unsigned int i = 0; i<Scores.size(); i++)
		{
			if(Scores[i]->Citizen == Citizen)
			{
				return Scores[i];
			}
		}
	}
	UserScore *s = new UserScore();
	s->Name = Name;
	s->Citizen = Citizen;
	Scores.push_back(s);
	return s;
}
char *WarBot::GetWinList()
{
	char *m = new char[AW_MAX_ATTRIBUTE_LENGTH];
	char usr[255];
	int CurrentMax = 0;
	int usrct = 0; //Number of printed users
	int uncnt = 0; //Number of unprinted users
	for(unsigned int i=0; i<Users.size(); i++)
	{
		if(i > Users.size()||Users[i]==NULL)
		{
		}
		else if(Users[i]->Score->Kills > CurrentMax)
		{
			CurrentMax = Users[i]->Score->Kills;
			sprintf(m,"Game led by %s",Users[i]->Name.c_str());
			usrct = 1;
			uncnt = 0;
		}
		else if (CurrentMax > 0 && Users[i]->Score->Kills == CurrentMax)
		{
			if(usrct ==3) //Max printable users
			{
				uncnt++;
			}
			else
			{
				sprintf(usr,", %s",Users[i]->Name.c_str());
				strcat(m,usr);
				usrct++;
			}
		}
	}
	if(usrct == 0)
	{
		sprintf(m,"Game led by nobody");
		return m;
	}
	if(uncnt > 0)
	{
		sprintf(usr," and %i others with %i kills",uncnt, CurrentMax);
		strcat(m,usr);
		return m;
	}
	sprintf(usr," with %i kills",CurrentMax);
	strcat(m,usr);
	return m;
}

void WarBot::OnUserEnter()
{

	User *U = new User(aw_int(AW_AVATAR_SESSION));
	U->Name = aw_string(AW_AVATAR_NAME);
	U->SearchName = aw_string(AW_AVATAR_NAME);
	boost::to_upper(U->SearchName);
	U->Citizen = aw_int(AW_AVATAR_CITIZEN);
	U->Position.X = aw_int(AW_AVATAR_X);
	U->Position.Y = aw_int(AW_AVATAR_Y);
	U->Position.Z = aw_int(AW_AVATAR_Z);
	U->Score = GetScore(U->Citizen,U->Name);

	Weapon *W = new Weapon();
	W->Name = "Paintball Gun";
	W->Damage = 9001.0f;
	U->Weapons.push_back(W);
	U->CurrentWeapon = 0;
	Users.push_back(U);
	aw_int_set(AW_TELEPORT_X,0);
	aw_int_set(AW_TELEPORT_Y,0);
	aw_int_set(AW_TELEPORT_Z,0);
	if(!((U->Position < GZMax) && (U->Position>GZMin)))
	{
		aw_teleport(U->Session);
	}
	aw_bool_set (AW_WORLD_ALLOW_PASSTHRU,false);
	aw_bool_set (AW_WORLD_ALLOW_FLYING, false);
	aw_world_attributes_send (U->Session);
}

void WarBot::OnUserChange()
{
	User *U = Find(aw_int(AW_AVATAR_SESSION));
	if(!U)
	{
		return;
	}
	U->Position.X = aw_int(AW_AVATAR_X);
	U->Position.Y = aw_int(AW_AVATAR_Y);
	U->Position.Z = aw_int(AW_AVATAR_Z);
	if((U->Position < GZMax) && U->Position>GZMin && U->Living == false)
	{
		U->Living = true;
		U->Health = 100.00;
	}
}
void WarBot::OnUserClick()
{
	User *Clicker = Find(aw_int(AW_AVATAR_SESSION));
	User *Clicked = Find(aw_int(AW_CLICKED_SESSION));
	if(!Clicker||!Clicked)
	{
		return;
	}
	if((Clicker->Position < GZMax) && (Clicker->Position>GZMin))
	{
		Say(Clicker->Session,"You cannot shoot while inside the safe zone");
		Debug("Error: Shot deferred, invoked in GZ");
		return;
	}
	if((Clicked->Position < GZMax) && (Clicked->Position>GZMin))
	{
		Say(Clicker->Session,"You cannot shoot someone in the safe zone");
		Debug("Error: Shot deferred, target in GZ");
		return;
	}
	float Tmp = Clicker->Distance(Clicked);
	if(Tmp >85.0f)
	{
		Say(Clicker->Session,"Target too far away!");
		Debug("Error: Shot deferred, Target %f meters away",Tmp);
		return;
	}
	if(Clicker->God)
	{
		Say(Clicker->Session,"You cannot shoot while godmode is enabled");
		return;
	}
	if(Clicked->God)
	{
		Say(Clicker->Session,"You cannot shoot someone who has godmode enabled");
		return;
	}
	if(!((Clicker->Position < FieldMax) && (Clicker->Position>FieldMin)))
	{
		Say(Clicker->Session,"You cannot shoot while outside the paintball field");
		Debug("Error: Shot deferred, invoked outside field (%i, %i, %i)",Clicker->Position.X,Clicker->Position.Y,Clicker->Position.Z);
		return;
	}
	if(!((Clicked->Position < FieldMax) && (Clicked->Position>FieldMin)))
	{
		Say(Clicker->Session,"You cannot shoot someone outside the paintball field");
		Debug("Error: Shot deferred, target outside field (%i, %i, %i)",Clicked->Position.X,Clicked->Position.Y,Clicked->Position.Z);
		return;
	}
	if(Clicker->Shoot(Clicked))
	{
		if(Clicked->Health <=0.0)
		{
			Clicker->Score->Kills++;
			Clicked->Score->Deaths++;
			Clicked->Living = false;
			Say(Clicker->Session,"You killed %s",Clicked->Name.c_str());
			Say(Clicked->Session,"you were killed by %s",Clicker->Name.c_str());
			aw_int_set(AW_TELEPORT_X,0);
			aw_int_set(AW_TELEPORT_Y,0);
			aw_int_set(AW_TELEPORT_Z,0);
			aw_int_set(AW_TELEPORT_YAW,0);
			aw_bool_set(AW_TELEPORT_WARP,0);
			aw_teleport(Clicked->Session);
			SortScores = true;
		}
	}
}
void WarBot::OnUserExit()
{
	int session = aw_int(AW_AVATAR_SESSION);
	for(unsigned int i=0; i<Users.size(); i++)
	{
		if(Users[i] == NULL)
			return;

		if(Users[i]->Session == session)
		{
			if(Users[i]->Score->Kills == 0 && Users[i]->Score->Deaths == 0)
			{
				for(unsigned int s=0; s<Scores.size(); s++)
				{
					if(Scores[s] == NULL)
						return;

					if(Scores[s]->Citizen == Users[i]->Citizen)
					{
						delete Scores[s];
						Scores.erase(Scores.begin()+s);
					}
				}
			}
			delete Users[i];
			Users.erase(Users.begin()+i);
		}
	}
}
void WarBot::OnChatMessage()
{

	User *U = Find(aw_int(AW_CHAT_SESSION));
	if(!U)
	{
		return;
	}
	string Message(aw_string(AW_CHAT_MESSAGE));
	vector<string> Token;

	boost::trim(Message);
	boost::to_upper(Message);

	boost::split(Token, Message, boost::is_any_of(" "), boost::token_compress_on );
	if(Token[0] == "/VERSION")
	{
		Say(U->Session,"%s i am AWSplat Paintball Version %i.%i, written by Epsilion",U->Name.c_str(),VER_MAJOR,VER_MINOR);
	}
	if(Token[0] == "SCORES")
	{
		if(SortScores)
		{
			int j=0;
			UserScore *val;
			for(unsigned int i = 1; i < Scores.size(); i++)
			{
				val = Scores[i];
				j = i - 1;

				while(j >= 0 && Scores[j]->Kills < val->Kills)
				{
					Scores[j + 1] = Scores[j];
					j = j - 1;
				}
				Scores[j + 1] = val;
			}
			SortScores = false;
		}

		for(unsigned int i =0; i<Scores.size(); i++)
		{
			Say(U->Session,"%i. %s with %i kills and %i deaths",i+1, Scores[i]->Name.c_str(),Scores[i]->Kills,Scores[i]->Deaths);
		}
	}
	if(Token[0] == "SCORE")
	{
		Say(U->Session,"You have %i kills and %i deaths (%f K/D)",U->Score->Kills,U->Score->Deaths, (U->Score->Deaths >0?((float)U->Score->Kills/(float)U->Score->Deaths):((float)U->Score->Kills/1.0f)));
		Say(U->Session,"%s",GetWinList());
	}
	if(Token[0] == "/RESTART")
	{
		if(!aw_has_world_right(U->Citizen,AW_WORLD_EJECT_RIGHT))
		{
			return;
		}
		if(Token[1] == "END")
		{
			Say(0,"Shutting down..");
			aw_wait(500);
			WarBot::Stop();
			Debug("Game was terminated by %s (%i)", U->Name.c_str(), U->Citizen);
			return;
		}
		Say(0,"Game Complete, Restarting..");
		Say(0,"%s",GetWinList());
		aw_wait(500);
		WarBot::Restart();
	}
	if(Token[0] == "/GOD")
	{
		if(time(0)-U->LastModeChange >=300)
		{
			if(U->God)
				U->God = false;
			else
				U->God = true;

			Say(U->Session,"God Mode %s",(U->God?"Enabled":"Disabled"));
			aw_bool_set (AW_WORLD_ALLOW_FLYING, U->God);
			aw_world_attributes_send (U->Session);
			U->LastModeChange = time(0);
		}
		else
		{
			Say(U->Session,"You cannot change your mode for %i more minutes",(int)(ceil((300-(time(0)-U->LastModeChange))/60.0f)));
		}
	}
}


void WarBot::Debug(char *Format,...)
{
#ifdef DEBUG
	va_list lptr;
	va_start(lptr,Format);
#ifdef DEBUG_AW
	char m[AW_MAX_ATTRIBUTE_LENGTH];
	vsnprintf(m,AW_MAX_ATTRIBUTE_LENGTH,Format,lptr);
	Say(0,m);
#else
	vprintf(Format,lptr);
	printf("\r\n");
#endif
	va_end(lptr);
#endif
}

void WarBot::Say(int Session,const char *Format,...)
{
	va_list lptr;
	va_start(lptr,Format);
	char m[AW_MAX_ATTRIBUTE_LENGTH];
	sprintf(m,"Paintball:\t");
	vsnprintf(&m[11],AW_MAX_ATTRIBUTE_LENGTH-11,Format,lptr);
	aw_string_set(AW_CONSOLE_MESSAGE,m);
	aw_int_set(AW_CONSOLE_RED,255);
	aw_int_set(AW_CONSOLE_BLUE,0);
	aw_int_set(AW_CONSOLE_GREEN,0);
	if(Session > 0)
	{
		aw_console_message(Session);
	}
	else
	{
		for(unsigned int i=0; i<Users.size(); i++)
		{
			aw_console_message(Users[i]->Session);
		}
	}
	va_end(lptr);
}

void WarBot::LoadConfig()
{
	ifstream Config;
	Config.open("./AWSplat.cfg");
	if(!Config)
		throw string("Could not find configuration file");
	vector<string> Token;
	string Line;
	while(Config.good())
	{
		getline(Config,Line);
		boost::trim(Line);
		boost::split(Token, Line, boost::is_any_of("\t"), boost::token_compress_on );
		Token.erase( std::remove_if( Token.begin(), Token.end(),
									 boost::bind( &std::string::empty, _1 ) ), Token.end() );
		if(Token.size() == 2)
		{
			if(Token[0] == "GZ_NorthWest")
			{
				GZMax = Vector3D(Token[1]);
			}
			if(Token[0] == "GZ_SouthEast")
			{
				GZMin = Vector3D(Token[1]);
			}
			if(Token[0] == "Field_NorthWest")
			{
				FieldMax = Vector3D(Token[1]);
			}
			if(Token[0] == "Field_SouthEast")
			{
				FieldMin = Vector3D(Token[1]);
			}
			if(Token[0] == "Login_Name")
			{
				LoginName = Token[1];
			}
			if(Token[0] == "Login_Citizen")
			{
				LoginOwner = boost::lexical_cast<unsigned int>(Token[1]);
			}
			if(Token[0] == "Login_PPW")
			{
				LoginPPW = Token[1];
			}
			if(Token[0] == "Login_World")
			{
				LoginWorld = Token[1];
			}
			if(Token[0] == "Round_Length")
			{
				RoundLength = boost::lexical_cast<unsigned int>(Token[1]);
			}
			if(Token[0] == "#" || boost::starts_with(Token[0],"#"))
			{
				//Single-line comment
			}
		}
	}
	Config.close();
}

void WarBot::Restart()
{
	Stop();
	Start();
}

void WarBot::Stop()
{
	aw_exit();
	aw_event_set(AW_EVENT_AVATAR_ADD,0);
	aw_event_set(AW_EVENT_AVATAR_CLICK,0);
	aw_event_set(AW_EVENT_AVATAR_CHANGE,0);
	aw_event_set(AW_EVENT_OBJECT_CLICK,0);
	aw_event_set(AW_EVENT_AVATAR_DELETE,0);
	aw_event_set(AW_EVENT_CHAT,0);
	aw_destroy();
}

