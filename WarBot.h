#ifndef WARBOT_H
#define WARBOT_H
#include <vector>
#include <string>

#include "User.h"

#include "Vector.h"


class WarBot
{
public:
    static vector<User*> Users;
    static vector<UserScore*> Scores;
    static bool SortScores;
    static Vector3D GZMin;
    static Vector3D GZMax;
    static Vector3D FieldMin;
    static Vector3D FieldMax;
    static string LoginName;
    static string LoginPPW;
    static string LoginWorld;
    static unsigned int LoginOwner;
    static unsigned int RoundLength;

    static void Initalize();
    static void Start();
    static User* Find(int session);
    static User* Find(string Name);
    static UserScore* GetScore(int Citizen, string Name);
    static char* GetWinList();


    static void OnUserEnter();
    static void OnUserChange();
    static void OnUserClick();
    static void OnUserExit();
    static void OnChatMessage();
    static void OnObjectClick();
    static void Debug(char *Format,...);
    static void Say(int Session, const char *Format,...);
    static void LoadConfig();
    static void Restart();
    static void Stop();

protected:
private:
};
#endif // WARBOT_H
