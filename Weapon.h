#ifndef WEAPON_H
#define WEAPON_H


class Weapon
{
    public:
        int ID;
        char *Name;
        int MaxAmmo;
        int Ammo;
        float Damage;
        float MaxRadius;
        float MinRadius;

        Weapon();
        virtual ~Weapon();
    protected:
    private:
};

#endif // WEAPON_H
