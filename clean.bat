@echo off
echo Cleaning Project

del /s /q *.save
del /s /q .\bin\debug\*.*
del /s .\obj\*.*
rd /q .\bin\debug
rd /q .\obj\debug
rd /q .\obj\release
rd /q .\obj\

echo Clean Complete