#include <stdio.h>
#include <aw.h>
#include <boost/signals.hpp>
#include "WarBot.h"

using namespace std;

int main()
{
    WarBot::Start();
    return 0;
}